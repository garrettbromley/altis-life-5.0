#include "..\..\script_macros.hpp"

// Private variables
private ["_type","_price","_amount","_diff","_name","_hideout"];

// If there is no item selected 
if ((lbCurSel 8801) isEqualTo -1) exitWith {hint localize "STR_Shop_Virt_Nothing"};

// Get the nearest flag that holds all the variables
_bMarket = (nearestObjects [getPosATL player, ["Flag_White_F"], 100]) select 0;

// Get the array of custom prices (only contains the overwritten prices)
_customPrices = _bMarket getVariable ["customPrices", []];

_item = lbData[8801,(lbCurSel 8801)];		// Type of item
_price = ctrlText 8806;

// Check if item exists already in custom array, find it
private _exists = false;
private _index = -1;
{ if (_x select 0 == _item) then { _exists = true; _index = _forEachIndex; }} forEach _customPrices;

// If it exists in current custom pricing, then update it, else add it
if (_exists) then {
	_customPrices set [_index, [_item, _price]];
} else {
	_customPrices pushBack [_item, _price];
};

_bMarket setVariable ["customPrices", _customPrices, true];

// Update the menu with new pricing
[] call life_fnc_BMupdateMenu;