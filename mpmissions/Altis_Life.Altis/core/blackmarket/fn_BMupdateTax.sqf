#include "..\..\script_macros.hpp"

// Private variables
private ["_type","_price","_amount","_diff","_name","_hideout"];

// Get the nearest flag that holds all the variables
_bMarket = (nearestObjects [getPosATL player, ["Flag_White_F"], 100]) select 0;

// Get the entered tax rate and check if it is a number
_taxRate = ctrlText 8806;
if (!([_taxRate] call TON_fnc_isnumber)) exitWith {hint localize "STR_Shop_Virt_NoNum";};

// Set the variable of the market tax rate
_bMarket setVariable ["taxRate", _taxRate, true];