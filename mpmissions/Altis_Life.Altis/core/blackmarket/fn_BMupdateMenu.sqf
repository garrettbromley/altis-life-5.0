#include "..\..\script_macros.hpp"
/*
    File: fn_menu_update.sqf
    Author: Bryan "Tonic" Boardwine

    Description:
    Update and fill the virtual shop menu.
*/
private ["_item_list","_shopItems","_price", "_displayName", "_bMarket", "_override"];
disableSerialization;

// Setup price override
_override = false;

// Setup control vars.
_item_list = CONTROL(8800, 8801);

// Purge list
lbClear _item_list;

// Get the shop items
_shopItems = M_CONFIG(getArray,"VirtualShops","black_market","items");

// Get the nearest flag that holds all the variables
_bMarket = (nearestObjects [getPosATL player, ["Flag_White_F"], 100]) select 0;

// Get the array of custom prices (only contains the overwritten prices)
_customPrices = _bMarket getVariable ["customPrices", []];

// Get the current tax rate (defaults to 5% as per config)
_taxRate = _bMarket getVariable ["taxRate", LIFE_SETTINGS(getNumber,"blackmarket_default_tax")];

// Update field with current value 
ctrlSetText [8803, _taxRate];

{
    _displayName = M_CONFIG(getText,"VirtualItems",_x,"displayName");   // Config Display Name
    _price = M_CONFIG(getNumber,"VirtualItems",_x,"buyPrice");          // Config Price

    // If there is a custom price for this item 
    if (_x in _customPrices && count _customPrices > 0) then { 
        private _index = -1;        // Placeholder
        private _itemName = _x;     // For passing _x into new forEach

        // Find the index of the item so we can get the price
        { if (_x select 0 == _itemName) then { _index = _forEachIndex } } forEach _customPrices;
          
        // Get the custom price
        _price = (_customPrices select _index) select 1;
    };
    
    // If we have a price on the item
    if (!(_price isEqualTo -1)) then {
        _item_list lbAdd format ["%1  ($%2)",(localize _displayName),[_price] call life_fnc_numberText];
        _item_list lbSetData [(lbSize _item_list)-1,_x];
        _item_list lbSetValue [(lbSize _item_list)-1,_price];
        _icon = M_CONFIG(getText,"VirtualItems",_x,"icon");
        if (!(_icon isEqualTo "")) then {
            _item_list lbSetPicture [(lbSize _item_list)-1,_icon];
        };
    };
} forEach _shopItems;

// Update the displayed tax rate
