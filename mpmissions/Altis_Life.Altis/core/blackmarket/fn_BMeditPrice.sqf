#include "..\..\script_macros.hpp"

// Private variables
private ["_type","_price","_amount","_diff","_name","_hideout"];

// If there is no item selected 
if ((lbCurSel 8801) isEqualTo -1) exitWith {hint localize "STR_Shop_Virt_Nothing"};

// Get item information
_type = lbData[8801,(lbCurSel 8801)];							// Type of item
_price = lbValue[8801,(lbCurSel 8801)];							// Price of item
_name = localize format ["%1", M_CONFIG(getText,"VirtualItems",_type,"displayName")];	// Name of the item

// Set the values of the menu
_display = findDisplay 8800;

// Change item title
_itemTitle = _display displayCtrl 8804;
ctrlSetText [8804, _name];

// Change default price 
_itemDefaultPrice = _display displayCtrl 8805;
ctrlSetText [8805, format ["Default Price: %1", [M_CONFIG(getNumber,"VirtualItems",_type,"buyPrice")] call life_fnc_numberText]];

// Change price box
_itemPrice = _display displayCtrl 8806;
ctrlSetText [8806, _price];