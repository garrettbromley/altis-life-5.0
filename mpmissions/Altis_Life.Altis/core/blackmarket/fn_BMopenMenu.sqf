#include "..\..\script_macros.hpp"
/*
    File: fn_openMenu.sqf
    Author: EagleByte
    Description:
    Initialize the Black Market Edit menu.
*/

// Get the black market
_bMarket = (nearestObjects [getPosATL player, ["Flag_White_F"], 50]) select 0;

// Make sure they own the point
private _group = _bMarket getVariable ["gangOwner",grpNull];
if (isNull _group || _group != group player) exitWith {titleText[localize "STR_GNOTF_NotYourMarket","PLAIN"]};

// Checks good, open the menu
createDialog "black_market_menu";

[] call life_fnc_BMupdateMenu;