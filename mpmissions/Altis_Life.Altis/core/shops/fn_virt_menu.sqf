#include "..\..\script_macros.hpp"
/*
    File: fn_virt_menu.sqf
    Author: Bryan "Tonic" Boardwine
    Description:
    Initialize the virtual shop menu.
*/

params [
    ["_shopNPC", objNull, [objNull]],
    "",
    "",
    ["_shopType", "", [""]]
];

if (isNull _shopNPC || {_shopType isEqualTo ""}) exitWith {};

private _shopSide = M_CONFIG(getText,"VirtualShops",_shopType,"side");

life_shop_type = _shopType;
life_shop_npc = _shopNPC;

private _exit = false;

if !(_shopSide isEqualTo "") then {
    private _flag = switch (playerSide) do {case west: {"cop"}; case independent: {"med"}; default {"civ"};};
    if !(_flag isEqualTo _shopSide) then {_exit = true;};
};

if (_exit) exitWith {};

private _conditions = M_CONFIG(getText,"VirtualShops",_shopType,"conditions");

if !([_conditions] call life_fnc_levelCheck) exitWith {hint localize "STR_Shop_Veh_NotAllowed";};

// If they are at a black market and it is not owned by them, then let them know they may be charged more per item, additional tax, and if they want to proceed
if (life_shop_type == "black_market") then {
    // Get the nearest flag that holds all the variables
    _bMarket = (nearestObjects [getPosATL player, ["Flag_White_F"], 100]) select 0;
    
    // Get the owner and tax rate of the point
    private _group = _bMarket getVariable ["gangOwner",grpNull];
    private _taxRate = _bMarket getVariable ["taxRate", LIFE_SETTINGS(getNumber,"blackmarket_default_tax")];
    
    // We will be charged extra (maybe)
    if (!isNull _group && _group != group player) then {
        // Ask if they want to purchase it or use the gang account
        _action = [
            format [localize "STR_Shop_Virt_BlackMarketWarning",[_taxRate] call life_fnc_numberText],
            localize "STR_Admin_Compensate",
            localize "STR_Global_Yes",
            localize "STR_Global_No"
        ] call BIS_fnc_guiMessage;

        // If the user pressed escape or if they don't want to proceed with purchasing here, exit
        if (!isNil "_action" && {!_action}) exitWith {};
    };
};

createDialog "shops_menu";

[] call life_fnc_virt_update;
