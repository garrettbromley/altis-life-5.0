#include "..\..\script_macros.hpp"
/*
    File: fn_virt_update.sqf
    Author: Bryan "Tonic" Boardwine

    Description:
    Update and fill the virtual shop menu.
*/
private ["_item_list","_gear_list","_shopItems","_name","_price", "_bMarket", "_override"];
disableSerialization;

// Setup price override
_override = false;

private _customPrices = [];
private _taxRate = 0;

// Setup control vars.
_item_list = CONTROL(2400,2401);
_gear_list = CONTROL(2400,2402);

// Purge list
lbClear _item_list;
lbClear _gear_list;

if (!isClass(missionConfigFile >> "VirtualShops" >> life_shop_type)) exitWith {closeDialog 0; hint localize "STR_NOTF_ConfigDoesNotExist";}; // Make sure the entry exists..
ctrlSetText[2403,localize (M_CONFIG(getText,"VirtualShops",life_shop_type,"name"))];
_shopItems = M_CONFIG(getArray,"VirtualShops",life_shop_type,"items");

// We are at the black market, so need to do some special pricing magic
if (life_shop_type == "black_market") then {
    // Get the nearest flag that holds all the variables
    _bMarket = (nearestObjects [getPosATL player, ["Flag_White_F"], 100]) select 0;

    // If they aren't a part of the group that owns the point, then they will get the custom prices
    private _group = _bMarket getVariable ["gangOwner",grpNull];
    if (_group != group player) then {
        // We will be overriding prices
        _override = true;

        // Get the array of custom prices (only contains the overwritten prices)
        _customPrices = _bMarket getVariable ["customPrices", []];

        // Get the current tax rate (defaults to 5% as per config)
        _taxRate = _bMarket getVariable ["taxRate", LIFE_SETTINGS(getNumber,"blackmarket_default_tax")];
    };
};

{
    _displayName = M_CONFIG(getText,"VirtualItems",_x,"displayName");   // Config Display Name
    _price = M_CONFIG(getNumber,"VirtualItems",_x,"buyPrice");          // Config Price

    // If we are overriding prices 
    if (_override) then {
        // If there is a custom price for this item 
        private _exists = false;
        { if ((_x select 0) in _customPrices) then { _exists = true } } forEach _customPrices;

        if (_exists && count _customPrices > 0) then { 
            private _index = -1;        // Placeholder
            private _itemName = _x;     // For passing _x into new forEach

            // Find the index of the item so we can get the price
            { if (_x select 0 == _itemName) then { _index = _forEachIndex } } forEach _customPrices;
            
            // Get the custom price
            _price = (_customPrices select _index) select 1;
        };

        // Add on tax rate if someone owns the point
        if (!isNull (_bMarket getVariable ["gangOwner",grpNull])) then {
            _price = _price + (_price * (_taxRate / 100));
        };
    };

    // If we have a price on the item
    if (!(_price < 0)) then {
        _item_list lbAdd format ["%1  ($%2)",(localize _displayName),[_price] call life_fnc_numberText];
        _item_list lbSetData [(lbSize _item_list)-1,_x];
        _item_list lbSetValue [(lbSize _item_list)-1,_price];
        _icon = M_CONFIG(getText,"VirtualItems",_x,"icon");
        if (!(_icon isEqualTo "")) then {
            _item_list lbSetPicture [(lbSize _item_list)-1,_icon];
        };
    };
} forEach _shopItems;

{
    _name = M_CONFIG(getText,"VirtualItems",_x,"displayName");
    _val = ITEM_VALUE(_x);

    if (_val > 0) then {
        _gear_list lbAdd format ["%2 [x%1]",_val,(localize _name)];
        _gear_list lbSetData [(lbSize _gear_list)-1,_x];
        _icon = M_CONFIG(getText,"VirtualItems",_x,"icon");
        if (!(_icon isEqualTo "")) then {
            _gear_list lbSetPicture [(lbSize _gear_list)-1,_icon];
        };
    };
} forEach _shopItems;
