#include "..\..\script_macros.hpp"
/*
    File: fn_virt_buy.sqf
    Author: Bryan "Tonic" Boardwine

    Description:
    Buy a virtual item from the store.
*/

// Private variables
private ["_type","_price","_amount","_diff","_name","_hideout"];

// If they didn't select anything
if ((lbCurSel 2401) isEqualTo -1) exitWith {hint localize "STR_Shop_Virt_Nothing"};

// Get the information on the item
_type = lbData[2401,(lbCurSel 2401)];
_price = lbValue[2401,(lbCurSel 2401)];
_amount = ctrlText 2404;

// If they didn't specify a number
if (!([_amount] call TON_fnc_isnumber)) exitWith {hint localize "STR_Shop_Virt_NoNum";};

// Make sure they can take the item (space)
_diff = [_type,parseNumber(_amount),life_carryWeight,life_maxWeight] call life_fnc_calWeightDiff;
_amount = parseNumber(_amount);
if (_diff <= 0) exitWith {hint localize "STR_NOTF_NoSpace"};
_amount = _diff;

// Hangouts stuff
private _altisArray = ["Land_u_Barracks_V2_F","Land_i_Barracks_V2_F"];
private _tanoaArray = ["Land_School_01_F","Land_Warehouse_03_F","Land_House_Small_02_F"];
private _hideoutObjs = [[["Altis", _altisArray], ["Tanoa", _tanoaArray]]] call TON_fnc_terrainSort;
_hideout = (nearestObjects[getPosATL player,_hideoutObjs,25]) select 0;
if ((_price * _amount) > CASH && {!isNil "_hideout" && {!isNil {group player getVariable "gang_bank"}} && {(group player getVariable "gang_bank") <= _price * _amount}}) exitWith {hint localize "STR_NOTF_NotEnoughMoney"};

// Keep them from spamming
if ((time - life_action_delay) < 0.2) exitWith {hint localize "STR_NOTF_ActionDelay";};
life_action_delay = time;

// Get the display name of the item
_name = M_CONFIG(getText,"VirtualItems",_type,"displayName");

// If they are able to take the item
if ([true,_type,_amount] call life_fnc_handleInv) then {
    
    // If they are at a hideout, part of a the gang and has the money for the item in the gang's bank
    if (!isNil "_hideout" && {!isNil {group player getVariable "gang_bank"}} && {(group player getVariable "gang_bank") >= _price}) then {
        
        // Ask if they want to purchase it or use the gang account
        _action = [
            format [(localize "STR_Shop_Virt_Gang_FundsMSG")+ "<br/><br/>" +(localize "STR_Shop_Virt_Gang_Funds")+ " <t color='#8cff9b'>$%1</t><br/>" +(localize "STR_Shop_Virt_YourFunds")+ " <t color='#8cff9b'>$%2</t>",
                [(group player getVariable "gang_bank")] call life_fnc_numberText,
                [CASH] call life_fnc_numberText
            ],
            localize "STR_Shop_Virt_YourorGang",
            localize "STR_Shop_Virt_UI_GangFunds",
            localize "STR_Shop_Virt_UI_YourCash"
        ] call BIS_fnc_guiMessage;
        
        // If they want to use the gang's money
        if (_action) then {
            hint format [localize "STR_Shop_Virt_BoughtGang",_amount,(localize _name),[(_price * _amount)] call life_fnc_numberText];
            _funds = group player getVariable "gang_bank";
            _funds = _funds - (_price * _amount);
            group player setVariable ["gang_bank",_funds,true];

            // Update the gang's funds
            if (life_HC_isActive) then {
                [1,group player] remoteExecCall ["HC_fnc_updateGang",HC_Life];
            } else {
                [1,group player] remoteExecCall ["TON_fnc_updateGang",RSERV];
            };

        } else {
            // If they didn't have enough money
            if ((_price * _amount) > CASH) exitWith {[false,_type,_amount] call life_fnc_handleInv; hint localize "STR_NOTF_NotEnoughMoney";};
            
            // They had enough money, so they purchased the item
            hint format [localize "STR_Shop_Virt_BoughtItem",_amount,(localize _name),[(_price * _amount)] call life_fnc_numberText];
            CASH = CASH - _price * _amount;
        };

    // They are not at a hideout, so normal transaction
    } else {
        // If they didn't have enough money
        if ((_price * _amount) > CASH) exitWith {hint localize "STR_NOTF_NotEnoughMoney"; [false,_type,_amount] call life_fnc_handleInv;};
        
        // They had enough money, so they purchased the item
        hint format [localize "STR_Shop_Virt_BoughtItem",_amount,(localize _name),[(_price * _amount)] call life_fnc_numberText];
        CASH = CASH - _price * _amount;
    };

    // Well, they purchased the item, so lets give the gang a cut of the money (tax)
    if (life_shop_type == "black_market") then {
        // Get the nearest flag that holds all the variables
        private _bMarket = (nearestObjects [getPosATL player, ["Flag_White_F"], 100]) select 0;

        // If someone runs it, they should get paid taxes and the profit margin
        private _group = _bMarket getVariable ["gangOwner",grpNull];

        // Somebody owns it and it isn't your group, so pay up bitches
        if (!isNull _group && _group != group player) then {
            // Get the array of custom prices (only contains the overwritten prices)
            private _customPrices = _bMarket getVariable ["customPrices", []];

            // If there is a custom price for this item 
            private _exists = false;
            private _customPrice = -1;
            { if ((_x select 0) in _customPrices) then { _exists = true; _customPrice = _x select 1; } } forEach _customPrices;

            // See how much profit they should get over default item price
            private _upCharge = 0;
            if (_exists) then { _upCharge = _customPrice - M_CONFIG(getNumber,"VirtualItems",_type,"buyPrice"); };

            // Get the current tax rate (defaults to 5% as per config)
            private _taxes = 0;
            private _taxRate = _bMarket getVariable ["taxRate", LIFE_SETTINGS(getNumber,"blackmarket_default_tax")];

            // Figure out how much in taxes they made
            private _taxProfit = _price - ((_price * 100) / 100 + _taxRate);

            // Give them the upcharge + tax profits
            private _totalProfit = (_upCharge + _taxProfit) * _amount;

            // If someone from the group is online, give them the profit 
            private _aPlayer = units _group select 0;
            if (!isNull _aPlayer) then {
                private _gangBank = _group getVariable "gang_bank";
                _group setVariable ["gang_bank", _gangBank + _totalProfit, true];
                
                // Update the gang's funds
                if (life_HC_isActive) then {
                    [1, _group] remoteExecCall ["HC_fnc_updateGang",HC_Life];
                } else {
                    [1, _group] remoteExecCall ["TON_fnc_updateGang",RSERV];
                };
            };
        };   
    };

    // Update the virtual item menu
    [] call life_fnc_virt_update;
};

// Update the client's information
[0] call SOCK_fnc_updatePartial;
[3] call SOCK_fnc_updatePartial;
