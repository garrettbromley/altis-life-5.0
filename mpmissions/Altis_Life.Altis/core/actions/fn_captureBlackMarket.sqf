#include "..\..\script_macros.hpp"
/*
    File: fn_captureBlackMarket.sqf
    Author: Bryan "Tonic" Boardwine
    Edited by: EagleByte
    Description:
    Blah blah.
*/

// Get the nearest flag that holds all the variables
private _bMarket = (nearestObjects [getPosATL player, ["Flag_White_F"], 25]) select 0;

// Get the group of the owner of the market
private _group = _bMarket getVariable ["gangOwner",grpNull];

// If the player is not in a gang
if (isNil {group player getVariable "gang_name"}) exitWith {titleText[localize "STR_GNOTF_CreateGang","PLAIN"];};

// If the player's gang already owns the market
if (_group == group player) exitWith {titleText[localize "STR_GNOTF_Controlled_Market","PLAIN"]};

// If someone is already capturing the point
if ((_bMarket getVariable ["inCapture",false])) exitWith {hint localize "STR_GNOTF_onePersonAtATime";};


// Privatize variables we will be using
private ["_action", "_cpRate"];

// If another group owns this market
if (!isNull _group) then {

    // Get the name of the gang, default to empty string
    _gangName = _group getVariable ["gang_name",""];
    
    // Ask if they want to capture this point, since someone else owns it
    _action = [
        format [localize "STR_GNOTF_AlreadyControlled_Market",_gangName],
        localize "STR_GNOTF_CurrentCapture_Market",
        localize "STR_Global_Yes",
        localize "STR_Global_No"
    ] call BIS_fnc_guiMessage;

    // Slower capture rate if someone already owns it
    _cpRate = 0.0045;
} else {
    // Nobody owns it, so make the capture rate quicker
    _cpRate = 0.0075;
};

// If the user pressed escape or if they don't want to proceed with capturing, exit with message
if (!isNil "_action" && {!_action}) exitWith {titleText[localize "STR_GNOTF_CaptureCancel","PLAIN"];};

// Lets proceed with an action
life_action_inUse = true;

// Setup the progress bar
disableSerialization;
private _title = localize "STR_GNOTF_Capturing_Market";         // Set the title of the progress bar
"progressBar" cutRsc ["life_progress","PLAIN"];                 // Initiate the dialog
private _ui = uiNamespace getVariable "life_progress";          // Get the dialog
private _progressBar = _ui displayCtrl 38201;                   // Get the progress bar
private _titleText = _ui displayCtrl 38202;                     // Get the title text of the progress bar dialog
_titleText ctrlSetText format ["%2 (1%1)...","%",_title];       // Set the title of the progress bar
_progressBar progressSetPosition 0.01;                          // Set the position of the progress bar to just over 0
private _cP = 0.01;                                             // Set the capture level to just over 0

// Infinite loop
for "_i" from 0 to 1 step 0 do {
    // If they aren't performing the capturing animation, make them do it again
    if (animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
        [player,"AinvPknlMstpSnonWnonDnon_medic_1",true] remoteExecCall ["life_fnc_animSync",RCLIENT];  // Sync it
        player switchMove "AinvPknlMstpSnonWnonDnon_medic_1";
        player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
    };

    // Sleep a bit
    uiSleep 0.26;

    // If we lost the progress bar, create it again (theoretically shouldn't happen, but this is Arma)
    if (isNull _ui) then {
        "progressBar" cutRsc ["life_progress","PLAIN"];         // Initiate the dialog
        _ui = uiNamespace getVariable "life_progress";          // Get the dialog
        _progressBar = _ui displayCtrl 38201;                   // Get the progress bar
        _titleText = _ui displayCtrl 38202;                     // Get the title text of the progress bar dialog
    };

    // Add some progress
    _cP = _cP + _cpRate;

    // Update the position
    _progressBar progressSetPosition _cP;
    
    // Update the text of the progress bar
    _titleText ctrlSetText format ["%3 (%1%2)...",round(_cP * 100),"%",_title];
    
    // So everyone can know that this point is in the process of being captured
    _bMarket setVariable ["inCapture",true,true];

    // If this point is captures or if the player dies, change the above so everyone knows that this point isn't being captured
    if (_cP >= 1 || !alive player) exitWith {_bMarket setVariable ["inCapture",false,true];};

    // If the player is being tazed, knocked out, or interrupts the capturing, change the above so everyone knows that this point isn't being captured
    if (life_istazed) exitWith {_bMarket setVariable ["inCapture",false,true];};
    if (life_isknocked) exitWith {_bMarket setVariable ["inCapture",false,true];};
    if (life_interrupted) exitWith {_bMarket setVariable ["inCapture",false,true];};
};

// Kill the UI display and check for various states
"progressBar" cutText ["","PLAIN"];

// Stop the animation
player playActionNow "stop";

// If they die, are tazed, knocked out, or restrained exit with some variables to  change
if (!alive player || life_istazed || life_isknocked || player getVariable ["restrained",false]) exitWith {life_action_inUse = false;_bMarket setVariable ["inCapture",false,true];};

// If they interrupted the process, exit with some variables to change and notify them that they canceled
if (life_interrupted) exitWith {life_interrupted = false; titleText[localize "STR_GNOTF_CaptureCancel","PLAIN"]; life_action_inUse = false;_bMarket setVariable ["inCapture",false,true];};

// No longer capturing
life_action_inUse = false;

// Let them know they captured it
titleText[localize "STR_GNOTF_Captured_Market","PLAIN"];

// Select a random flag texture to set
private _flagTexture = [
    "\A3\Data_F\Flags\Flag_red_CO.paa",
    "\A3\Data_F\Flags\Flag_green_CO.paa",
    "\A3\Data_F\Flags\Flag_blue_CO.paa",
    "\A3\Data_F\Flags\Flag_white_CO.paa",
    "\A3\Data_F\Flags\flag_fd_red_CO.paa",
    "\A3\Data_F\Flags\flag_fd_green_CO.paa",
    "\A3\Data_F\Flags\flag_fd_blue_CO.paa",
    "\A3\Data_F\Flags\flag_fd_orange_CO.paa"
] call BIS_fnc_selectRandom;

// Set the texture of the flag to that we selected
_this select 0 setFlagTexture _flagTexture;

// Let the world know we captured this point
[[0,1],"STR_GNOTF_CaptureSuccess_Market",true,[name player,(group player) getVariable "gang_name"]] remoteExecCall ["life_fnc_broadcast",RCLIENT];

// Nobody is capturing this point right now
_bMarket setVariable ["inCapture",false,true];

// Set the market owner to the point
_bMarket setVariable ["gangOwner",group player,true];
