class black_market_menu {
    idd = 8800;
    name= "black_market_menu";
    movingEnable = 0;
    enableSimulation = 1;

    class controlsBackground {
        class Life_RscTitleBackground: Life_RscText {
            colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", "(profilenamespace getvariable ['GUI_BCG_RGB_A',0.7])"};
            idc = -1;
            x = 0.1;
            y = 0.2;
            w = 0.8;
            h = (1 / 25);
        };

        class MainBackground: Life_RscText {
            colorBackground[] = {0, 0, 0, 0.7};
            idc = -1;
            x = 0.1;
            y = 0.2 + (11 / 250);
            w = 0.8;
            h = 0.6 - (22 / 250);
        };

        class vasText: Life_RscText {
            idc = -1;
            colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", 0.5};
            text = "$STR_VS_SI";
            sizeEx = 0.04;
            x = 0.12;
            y = 0.27;
            w = 0.350;
            h = 0.04;
        };

        class vasgText: Life_RscText {
            idc = -1;
            colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", 0.5};
            text = "$STR_VS_TAX";
            sizeEx = 0.04;
            x = 0.53;
            y = 0.27;
            w = 0.350;
            h = 0.04;
        };
		
		class vaspText: Life_RscText {
            idc = -1;
            colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", 0.5};
            text = "$STR_MAR_Black_Market_Selected";
            sizeEx = 0.04;
            x = 0.53;
            y = 0.45;
            w = 0.350;
            h = 0.04;
        };
    };

    class controls {
        class itemList: Life_RscListBox {
            idc = 8801;
            text = "";
            onLBSelChanged = "_this call life_fnc_BMeditPrice";
            sizeEx = 0.030;
            x = 0.12;
            y = 0.31;
            w = 0.350;
            h = 0.380;
        };

        class Title: Life_RscTitle {
            colorBackground[] = {0, 0, 0, 0};
            idc = 8802;
            text = "$STR_MAR_Black_Market_Manager";
            x = 0.1;
            y = 0.2;
            w = 0.8;
            h = (1 / 25);
        };

        class taxEdit: Life_RscEdit {
            idc = 8803;
            text = "";
            sizeEx = 0.030;
            x = 0.55;
            y = 0.34;
            w = 0.08;
            h = 0.03;
        };
		
		class taxPercent : Life_RscText {
			idc = -1;
            text = "%";
            sizeEx = 0.04;
            x = 0.65;
            y = 0.34;
            w = 0.16;
            h = 0.03;		
		};

        class ButtonUpdateTax: Life_RscButtonMenu {
            idc = -1;
            text = "$STR_VS_Set";
            colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", 0.5};
            onButtonClick = "[] call life_fnc_BMupdateTax";
            x = 0.724;
            y = 0.335;
            w = (6.25 / 40);
            h = (1 / 25);
        };
		
		class itemName : Life_RscText {
			idc = 8804;
            text = "";
            sizeEx = 0.04;
            x = 0.53;
            y = 0.505;
            w = 0.350;
            h = 0.03;		
		};
		
		class itemDefaultPrice : Life_RscText {
			idc = 8805;
            text = "";
            sizeEx = 0.04;
            x = 0.53;
            y = 0.55;
            w = 0.350;
            h = 0.03;		
		};
		
		class itemDollar : Life_RscText {
			idc = -1;
            text = "$";
            sizeEx = 0.04;
            x = (0.53 + (0.35 / 2) - ((6.25 / 40) / 2)) - 0.025;
            y = 0.65;
            w = 0.16;
            h = 0.03;		
		};
		
		class priceEdit: Life_RscEdit {
            idc = 8806;
            text = "";
            sizeEx = 0.030;
            x = 0.53 + (0.35 / 2) - ((6.25 / 40) / 2);
            y = 0.65;
            w = (6.25 / 40);
            h = 0.03;
        };

        class ButtonUpdatePrice: Life_RscButtonMenu {
            idc = -1;
            text = "$STR_VS_Set";
            colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", 0.5};
            onButtonClick = "[] call life_fnc_BMupdatePrice";
            x = 0.53 + (0.35 / 2) - ((6.25 / 40) / 2);
            y = 0.70;
            w = (6.25 / 40);
            h = (1 / 25);
        };

        class ButtonClose: Life_RscButtonMenu {
            idc = -1;
            text = "$STR_Global_Close";
            onButtonClick = "closeDialog 0;";
            x = 0.1;
            y = 0.8 - (1 / 25);
            w = (6.25 / 40);
            h = (1 / 25);
        };
    };
};